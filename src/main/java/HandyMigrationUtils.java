import com.enonic.cms.api.client.ClientException;
import com.enonic.cms.api.client.ClientFactory;
import com.enonic.cms.api.client.RemoteClient;
import com.enonic.cms.api.client.model.*;
import jxl.CellView;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;


public class HandyMigrationUtils {

    static RemoteClient client;
    final static XMLOutputter xmlout = new XMLOutputter(Format.getPrettyFormat());
    final static Logger LOG = LoggerFactory.getLogger(HandyMigrationUtils.class);
    static Properties props = new Properties();

    final static Scanner reader = new Scanner(System.in);

    static {
        try {
            //disableSslVerification();
            props.load(HandyMigrationUtils.class.getClassLoader().getResourceAsStream("HandyMigrationUtils.properties"));
        }
        catch (Exception e){}
    }

    public static void main(String args[]) throws Exception{
        String rpcBinEndpoint = getPropertyOrUserinput("rpcBinEndpoint");
        client = ClientFactory.getRemoteClient(rpcBinEndpoint);
        try {
            client.login(getPropertyOrUserinput("username"), getPropertyOrUserinput("password"));
        }catch (ClientException ce){
            System.out.println("Enter correct username / password in HandyMigrationUtils.property or remove default values to be promted runtime.");
            throw ce;
        }
        //deleteCategories();
        //getListOfAllContenttypesWithContentForTopCategory();
        getPageTemplateReportsForMigrationExcel();
        //testExcel();
        reader.close();

    }

    private static String getPropertyOrUserinput(String propertyName) throws IOException {
        String propertyValue = (String)props.get(propertyName);
        if (propertyValue == null || "".equals(propertyValue)) {
            String propertyNamePrompt = propertyName+"Prompt";
            System.out.println((String)props.get(propertyNamePrompt));
            propertyValue = reader.next();
        }
        return propertyValue;
    }

    private static void testExcel() throws Exception{

        WorkbookSettings ws = new WorkbookSettings();
        ws.setLocale(new Locale("no", "NO"));
        WritableWorkbook workbook = Workbook.createWorkbook(new File("report4.xls"));
        CellFormat dataCell = new WritableCellFormat();
        WritableCellFormat headingCellFormat = new WritableCellFormat();
        headingCellFormat.setBackground(Colour.GRAY_25);
        headingCellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        CellFormat headingCell = headingCellFormat;

        WritableSheet sheetNo = workbook.createSheet("No", 0);
        WritableSheet sheetSe = workbook.createSheet("Se", 0);

        Label l = new Label(0,0, "Norsk", headingCell);
        l.setString("Norsk");
        sheetNo.addCell(l);
        sheetNo.insertRow(1);
        l = new Label(0,1,"Norsk2", dataCell);
        sheetNo.addCell(l);

        Label l2 = new Label(0,0, "Svensk", dataCell);
        l2.setString("Svensk");
        sheetSe.addCell(l2);

        workbook.write();
        workbook.close();

    }

    private static String getAttributeValue(Element element, String name){
        if (element == null || name == null || element.equals("") || name.equals("")){
            return "";
        }
        try{
            return element.getAttribute(name).getValue();
        }catch (Exception e){
            return "";
        }
    }

    private static void getPageTemplateReportsForMigrationExcel() throws Exception{

        List<String> sites2Migrate = Arrays.asList(getPropertyOrUserinput("sitesToMigrate").split(","));
        int numberOfPagesPerSitetoMigrage = Integer.parseInt(getPropertyOrUserinput("numberOfPagesPerSiteToMigrate"));

        //Excel setup (jxl)
        WorkbookSettings ws = new WorkbookSettings();
        ws.setLocale(new Locale("no", "NO"));
        WritableWorkbook workbook = Workbook.createWorkbook(new File("page-template-report-migration"+ System.currentTimeMillis() +".xls"));
        Iterator<String> sitesIt = sites2Migrate.iterator();
        while(sitesIt.hasNext()){
            String site = sitesIt.next();
            System.out.println("Generating report for site " + site + " .... ");
            getPageTemplateReportsForMigration(Integer.parseInt(site), workbook, numberOfPagesPerSitetoMigrage);
        }

        //Format columns to expand to content size
        WritableSheet[] writableSheets = workbook.getSheets();
        for (int i = 0; i < writableSheets.length;i++){
            WritableSheet wsheet = writableSheets[i];
            SheetSettings ss = wsheet.getSettings();
            ss.setVerticalFreeze(1);
            int numColumns = wsheet.getColumns();
            for (int c = 0; c < numColumns; c++){
                CellView cw = wsheet.getColumnView(c);
                cw.setAutosize(true);
                wsheet.setColumnView(c, cw);
            }

        }
        workbook.write();
        workbook.close();
    }

    private static void getPageTemplateReportsForMigration(int menuKey, WritableWorkbook workbook, int pageCount) throws Exception{


        //Excel setup (jxl)
        WritableCellFormat dataCellFormat = new WritableCellFormat();
        dataCellFormat.setWrap(false);
        CellFormat dataCell = dataCellFormat;

        WritableCellFormat headingCellFormat = new WritableCellFormat();
        headingCellFormat.setWrap(false);
        headingCellFormat.setBackground(Colour.GRAY_25);
        headingCellFormat.setLocked(true);
        headingCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        CellFormat headingCell = headingCellFormat;

        GetMenuParams getMenuParams = new GetMenuParams();
        getMenuParams.menuKey = menuKey;
        getMenuParams.levels = 99;
        getMenuParams.includeHidden=true;
        getMenuParams.tagItem=0;
        Document menu = client.getMenu(getMenuParams);

        //printDoc(menu);

        List menuItemsList = XPath.selectNodes(menu, "//menuitem");
        Iterator<Element> menuItemsListIt = menuItemsList.iterator();
        String headerArray[] = new String[]{"siteName","siteKey","contentKey","display-name","name", "path", "page-type","visible","page-template-key","page-template","page-template-type"};
        int headerCount = 0;


        WritableSheet sheet = workbook.createSheet("site " + menuKey, 0);


        while (headerCount < headerArray.length){
            addCell(sheet,headerCount,0,headerArray[headerCount], headingCell);
            headerCount++;
        }

        //ArrayList<String> moduleTypes = new ArrayList<String>(Arrays.asList("article-kundevalg","bable-reply-button","category-list","category-list-config","file","gpluss-module-minbil","hjelp-meg-node","image","module-advice-action-list","module-benefit-matrix","module-call-to-action","module-campaign","module-campaign-pluss","module-charts","module-dividend-calculator","module-faq","module-feed","module-feed-download-center","module-form-link-list","module-googlemap-image","module-icon-link-list","module-image","module-insurance-adviser","module-link-list","module-loan-application","module-map-locations","module-multimodule","module-nettbutikk","module-nettbutikk-partnerbanner","module-overview","module-quiz","module-react","module-react-json","module-ri-advanced","module-smart-module","module-theme","module-tip-and-advice","module-uvp-list","module-video","product-action-list-config","simple-article","texts"));
        Set<String> moduleTypesSet = new TreeSet<String>();
        Map<String, List> pageKeyModulesMap = new HashMap();
        int count = 0;
        System.out.println("Adding content with following contentkeys to report: ");
        while (menuItemsListIt.hasNext() && count < pageCount){//un-comment for testing n rows (saves time when testing)
            Element menuItem = menuItemsListIt.next();
            String key = menuItem.getAttribute("key").getValue();
            GetContentBySectionParams getContentBySectionParams = new GetContentBySectionParams();
            getContentBySectionParams.childrenLevel = 1;
            getContentBySectionParams.count = 5;
            getContentBySectionParams.includeData = false;
            getContentBySectionParams.includeVersionsInfo = false;
            getContentBySectionParams.includeOfflineContent = false;
            getContentBySectionParams.levels = 1;
            getContentBySectionParams.menuItemKeys = new int[]{Integer.parseInt(key)};

            Document contentBySectionDoc = client.getContentBySection(getContentBySectionParams);

            ArrayList<String> moduleList = new ArrayList<String>();

            //Add info about type of content directly published to the page
            List<Attribute> contentTypes = XPath.selectNodes(contentBySectionDoc, "//contents/content/@contenttype");
            Iterator<Attribute> contentTypesAttr = contentTypes.iterator();
            ArrayList<String> contentTypeList = new ArrayList<String>();
            while (contentTypesAttr.hasNext()){
                Attribute attr = contentTypesAttr.next();
                moduleList.add(attr.getValue().toString());//Add to list of modules for section content
                moduleTypesSet.add(attr.getValue().toString());//Add to list of existing modules for all content
            }

            //Add info about relatedcontent for content published to the page
            List<Attribute> relatedContents = XPath.selectNodes(contentBySectionDoc, "//relatedcontentkeys/relatedcontentkey/@contenttype");
            Iterator<Attribute> relatedContentsAttr = relatedContents.iterator();

            while (relatedContentsAttr.hasNext()){
                Attribute attr = relatedContentsAttr.next();
                moduleList.add(attr.getValue().toString());//Add to list of modules for section content
                moduleTypesSet.add(attr.getValue().toString());//Add to list of existing modules for all content
            }

            pageKeyModulesMap.put(key, moduleList);
            count++;
            System.out.print(" " + key);
        }
        System.out.println("");

        Iterator<String> moduleTypesSetIt = moduleTypesSet.iterator();
        System.out.println("Adding following report headers for current site (shows usage of contenttypes per page)");
        while (moduleTypesSetIt.hasNext()){
            String customContentType = moduleTypesSetIt.next();
            System.out.println(" " + customContentType);
            addCell(sheet,headerCount,0, customContentType, headingCell);
            headerCount++;
        }

        Set<String> keys = pageKeyModulesMap.keySet();
        Iterator<String> keysIt = keys.iterator();
        int cellCount = 0;
        int rowCount = 2;
        while (keysIt.hasNext()){
            String key = keysIt.next();
            Document menuItemDetailsDoc = getMenuItem(key);

            Document content = getDocWithMenuPath(key);
            List menuItems = XPath.selectNodes(content, "//menuitems/menuitem[@path='true']");
            Iterator<Element> menuItemsIt = menuItems.iterator();
            String path = "";
            while(menuItemsIt.hasNext()){
                Element menuItem = menuItemsIt.next();
                path+="/"+menuItem.getChild("name").getValue();
                if (menuItem.getAttribute("key").getValue().equals(key)) break;//only add to path current menuitem and above
            }

            Element menuItemDetailsEl = (Element)XPath.selectSingleNode(menuItemDetailsDoc, "//menuitem");
            Element pageDoc = (Element)XPath.selectSingleNode(menuItemDetailsEl, "page");
            addCell(sheet, cellCount++, rowCount, ((Element)XPath.selectSingleNode(menu, "//menus/menu/name")).getValue().toString(), dataCell);
            addCell(sheet, cellCount++, rowCount, ""+menuKey, dataCell);
            addCell(sheet, cellCount++, rowCount, ""+key, dataCell);
            addCell(sheet, cellCount++, rowCount, menuItemDetailsEl.getChild("display-name").getValue(), dataCell);
            addCell(sheet, cellCount++, rowCount, menuItemDetailsEl.getChild("name").getValue(), dataCell);
            addCell(sheet, cellCount++, rowCount, path, dataCell);
            addCell(sheet, cellCount++, rowCount, getAttributeValue(menuItemDetailsEl, "type"), dataCell);
            addCell(sheet, cellCount++, rowCount, getAttributeValue(menuItemDetailsEl, "visible"), dataCell);
            addCell(sheet, cellCount++, rowCount, getAttributeValue(pageDoc, "pagetemplatekey"), dataCell);
            addCell(sheet, cellCount++, rowCount, getAttributeValue(pageDoc, "pagetemplate"), dataCell);
            addCell(sheet, cellCount++, rowCount, getAttributeValue(pageDoc, "pagetemplatetype"), dataCell);

            //Iterate existing modules and list frequency for this page

            List<String> pageKeyModulesList = pageKeyModulesMap.get(key);
            Iterator<String> pageModulesIt = moduleTypesSet.iterator();
            while (pageModulesIt.hasNext()){
                String type = pageModulesIt.next();
                addCell(sheet, cellCount++, rowCount, ""+Collections.frequency(pageKeyModulesList, type), dataCell);
            }
            rowCount++;
            cellCount = 0;

        }

    }

    private static void addCell(WritableSheet sheet, int cellCount, int rowCount, String value, CellFormat cf) throws Exception{
        Label l = new Label(cellCount,rowCount, value, cf);
        l.setString(value);
        sheet.addCell(l);
    }

    private static Document getMenuItem(String key) throws Exception{
        GetMenuItemParams getMenuItemParams = new GetMenuItemParams();
        getMenuItemParams.menuItemKey = Integer.parseInt(key);
        getMenuItemParams.details = true;
        return client.getMenuItem(getMenuItemParams);
    }


    private static void getListOfAllContenttypesWithContentForTopCategory() throws Exception {
        Integer categoryKey = Integer.parseInt(getPropertyOrUserinput("listAllContenttypesWithContentBelowThisTopCategory"));
        Document doc = getCategories(categoryKey);
        Set<Integer> categoryContentTypeKeys = new HashSet<Integer>();
        List<Element> categoryElements = XPath.selectNodes(doc, "//category");
        Iterator<Element> catElIt = categoryElements.iterator();
        Map<Integer,String> contenttypeKeyName = new HashMap<>();
        while (catElIt.hasNext()){
            try {
                Element categoryEl = catElIt.next();
                Integer contenttypeKey = Integer.parseInt(getAttributeValue(categoryEl,"contenttypekey"));

                if (contenttypeKey==null || "".equals(contenttypeKey))continue;

                GetContentTypeConfigXMLParams getContentTypeConfigXMLParams = new GetContentTypeConfigXMLParams();
                getContentTypeConfigXMLParams.key=contenttypeKey;
                Document contenttypeDoc = client.getContentTypeConfigXML(getContentTypeConfigXMLParams);
                String contenttype = getAttributeValue((Element)XPath.selectSingleNode(contenttypeDoc,"moduledata/config"), "name");

                if (!categoryContentTypeKeys.contains(contenttypeKey)){
                    categoryContentTypeKeys.add(contenttypeKey);
                    if (!"".equals(contenttype)) {
                        contenttypeKeyName.put(contenttypeKey, contenttype);
                    }
                }

            }catch (Exception e){
                //LOG.warn("Category without contenttypekey..");
            }
        }

        Iterator<Integer> categoryContentTypeKeysIt = categoryContentTypeKeys.iterator();
        while (categoryContentTypeKeysIt.hasNext()){
            Integer contenttypekey = categoryContentTypeKeysIt.next();

            GetContentByCategoryParams getContentByCategoryParams = new GetContentByCategoryParams();
            getContentByCategoryParams.count=1;
            getContentByCategoryParams.levels=0;
            getContentByCategoryParams.query="contenttypekey = " + contenttypekey;
            getContentByCategoryParams.categoryKeys = new int[]{categoryKey};
            getContentByCategoryParams.includeOfflineContent=true;
            Document contentByCategoryDoc = client.getContentByCategory(getContentByCategoryParams);
            String totalcount = getAttributeValue(contentByCategoryDoc.getRootElement(), "totalcount");

            String contentTypeName = contenttypeKeyName.get(contenttypekey);
            if (contentTypeName==null || "".equals(contentTypeName)) contentTypeName = "contenttypeKey-"+contenttypekey + " (without name)";
            System.out.println(contentTypeName + ": "+ totalcount);

        }
    }

    private static Document getDocWithMenuPath(String key){
        GetMenuBranchParams params = new GetMenuBranchParams();
        params.menuItemKey = Integer.parseInt(key);
        params.includeTopLevel=false;
        params.includeHidden=true;
        return client.getMenuBranch(params);
    }

    private static void deleteCategories() throws Exception{

        List<String> categoryKeys = Arrays.asList(getPropertyOrUserinput("categoryKeysToDelete").split(","));
        DeleteCategoryParams deleteCategoryParams = new DeleteCategoryParams();
        deleteCategoryParams.includeContent = true;
        deleteCategoryParams.recursive = true;
        Iterator<String> categoryKeysIt = categoryKeys.iterator();
        while (categoryKeysIt.hasNext()){
            String categoryKey = categoryKeysIt.next();
            deleteCategoryParams.key = Integer.parseInt(categoryKey);
            try {
                client.deleteCategory(deleteCategoryParams);
                System.out.println("Deleted category with key " + categoryKey);
            }catch (ClientException e){
                LOG.error("Exception {}",e);
            }
        }
    }

    private static Document getCategories(int categoryKey) throws IOException {
        GetCategoriesParams getCategoriesParams = new GetCategoriesParams();
        getCategoriesParams.categoryKey = categoryKey;
        getCategoriesParams.levels=0;
        return client.getCategories(getCategoriesParams);
    }

    private static void printDoc(Document doc) throws Exception{
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(doc,System.out);
    }
    private static void printEl(Element el) throws Exception{
        if (el==null)return;
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(el,System.out);
    }

    private static void disableSslVerification() throws Exception{
        try
        {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
}
