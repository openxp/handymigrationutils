#Handy Remote Migration Tools cms2xp

This is a collection of methods that have been handy during migrating from cms to xp by utilizing the
Enonic CMS remote API (rpc endpoint). 

##HowTo
I import the code in IntelliJ. In the main() method of HandyMigrationUtils.java I call the method
that I wish to use and uncomment the others, and I just run the HandyMigrationUtils in the intelliJ GUI.

##Methods

###getPageTemplateReportsForMigrationExcel()
This method generates an excel report of existing sites, one tab in excel per site. It lists all
pages with full path, and also it lists type of page template that is in use. It also lists what
contenttypes are published to each page and its relatedcontent and each relatedcontents frequency 
on the page. This data is customized for one customer, but the code can be modified to extract the
kind of data you are interested in.


###getListOfAllContenttypesWithContentForTopCategory()
Enter a parent category key and recursively list all contenttypes that have content, also print how many 
content exists of each contenttype

###getPropertyOrUserinput()
This method allows you to configure properties in the HandyMigrationUtils.properties file and extract 
them in other methods.


##Utility methods

###getAttributeValue()
Get the value of an attribute on a xml element

##Helper / testing methods

###testExcel()
For testing excel functionality with jxl library

###disableSslVerification()
This was a method I used to disable ssl verfication when one of the rpc endpoints had a unvalid SSL cert

###printDoc()
Prettyprints xml document